package com.spotpromo.spotcamera.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.spotpromo.spotcamera.R
import com.spotpromo.spotcamera.databinding.ItemGallery2Binding
import com.spotpromo.spotcamera.databinding.LayoutSpotCameraBinding

class RecyclerGalleryCamera(val activity: AppCompatActivity, val items: ArrayList<String>) : RecyclerView.Adapter<RecyclerGalleryCamera.ViewModel>() {
    private lateinit var binding: ItemGallery2Binding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        binding = ItemGallery2Binding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewModel(binding.root)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val item = items[position]
        with(holder.itemView) {
            Glide.with(this)
                .load(item)
                .into(binding.ivImageCamera)

            binding.ivImageCamera.setOnClickListener {
                val clickImage = activity as ClickImageCamera
                clickImage.onSelectImage(item)
            }
        }
    }

    interface ClickImageCamera {
        fun onSelectImage(image: String)
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view)
}