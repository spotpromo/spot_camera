package com.spotpromo.spotcamera.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.spotpromo.spotcamera.R
import com.spotpromo.spotcamera.databinding.ItemGalleryBinding
import com.spotpromo.spotcamera.model.FotoGaleria
import java.io.File

class RecyclerListGallery (val activity: AppCompatActivity, val items: ArrayList<FotoGaleria>) : RecyclerView.Adapter<RecyclerListGallery.ViewModel>() {
    private lateinit var binding: ItemGalleryBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        binding = ItemGalleryBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewModel(binding.root)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val item = items[position]
        if (item.isFolder) {
            with(holder.itemView) {
                binding.tvTitulo.visibility = View.VISIBLE

                binding.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_folder))

                binding.ivImage.setOnClickListener {
                    val clickImage = activity as ClickImage
                    clickImage.onSelectFolder(item.dataFile!!)
                }

                binding.tvTitulo.text = retornaFolder(item.dataFile!!)
            }
        } else {
            with(holder.itemView) {
                binding.tvTitulo.visibility = View.GONE
                Glide.with(this)
                    .load(item.dataFile)
                    .into( binding.ivImage)

                binding.ivImage.setOnClickListener {
                    val clickImage = activity as ClickImage
                    clickImage.onSelectImage(item.dataFile!!)
                }
            }

        }
    }

    interface ClickImage {
        fun onSelectImage(image: String)
        fun onSelectFolder(folder: String)
    }

    fun retornaFolder(caminhoFoto: String): String {
        val foto = caminhoFoto.split("/")

        return foto[foto.size - 1]
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view)
}