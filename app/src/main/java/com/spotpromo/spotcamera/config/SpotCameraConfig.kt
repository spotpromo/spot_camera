package com.spotpromo.spotcamera.config

import com.spotpromo.spotcamera.utils.ImagemUtil


object SpotCameraConfig {
    var buttonColor = "#263552"
    var photoSize = "1024x768"
    var tempFilePath = "temp_file.jpeg"
    //var tempFilePath = ImagemUtil.CriarFileTemporarioStringRoteiro("spotfotos", "temp_file.jpeg", 0)
    var isWithGallery = false
}