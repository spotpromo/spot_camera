package com.spotpromo.spotcamera.utils

import android.view.View

object SpotUtils {

    fun View.visibility(visible: Boolean) {
        visibility = if (visible) View.VISIBLE else View.GONE
    }
}