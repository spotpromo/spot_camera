package com.spotpromo.spotcamera.gallery

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.spotpromo.spotcamera.R
import com.spotpromo.spotcamera.adapter.RecyclerListGallery
import com.spotpromo.spotcamera.config.SpotCameraConfig
import com.spotpromo.spotcamera.databinding.LayoutSpotCameraBinding
import com.spotpromo.spotcamera.databinding.LayoutSpotGalleryBinding
import com.spotpromo.spotcamera.model.FotoGaleria
import com.spotpromo.spotcamera.utils.Alerta
import com.spotpromo.spotcamera.utils.ImagemUtil
import com.spotpromo.spotcamera.utils.RecycleItemDecoration
import id.zelory.compressor.Compressor
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.io.File
import java.lang.Exception


class SpotGallery : AppCompatActivity(), RecyclerListGallery.ClickImage {
    private val GALLERY_REQUEST_CODE = 100
    var allFolder: java.util.ArrayList<FotoGaleria>? = java.util.ArrayList<FotoGaleria>()
    var listImageByFolder: HashMap<String, ArrayList<String>>? = HashMap<String, ArrayList<String>>()
    var nivel = 0
    var mRecycle: RecyclerListGallery? = null
    private lateinit var binding: LayoutSpotGalleryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutSpotGalleryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        onCheckedPermission()
        setUpToolbar()
    }
    private fun onCheckedPermission() {
        if (ContextCompat.checkSelfPermission(
                this@SpotGallery,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(
                this@SpotGallery, getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_camera),
                resources.getString(R.string.btn_permitir),
                { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@SpotGallery,
                        arrayOf(Manifest.permission.CAMERA),
                        GALLERY_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                { dialog, which ->
                    onBackPressed()
                }, false
            )
            return
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this@SpotGallery,
                    Manifest.permission.READ_MEDIA_IMAGES
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Alerta.show(
                    this@SpotGallery, getString(R.string.msg_atencao),
                    resources.getString(R.string.msg_permissao_arquivos),
                    resources.getString(R.string.btn_permitir),
                    { dialog, which ->
                        ActivityCompat.requestPermissions(
                            this@SpotGallery,
                            arrayOf(Manifest.permission.READ_MEDIA_IMAGES),
                            GALLERY_REQUEST_CODE
                        )
                    },
                    resources.getString(R.string.btn_cancelar),
                    { dialog, which ->
                        onBackPressed()
                    }, false
                )
                return
            }

        } else {
            if (ContextCompat.checkSelfPermission(
                    this@SpotGallery,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Alerta.show(
                    this@SpotGallery, getString(R.string.msg_atencao),
                    resources.getString(R.string.msg_permissao_arquivos),
                    resources.getString(R.string.btn_permitir),
                    { dialog, which ->
                        ActivityCompat.requestPermissions(
                            this@SpotGallery,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            GALLERY_REQUEST_CODE
                        )
                    },
                    resources.getString(R.string.btn_cancelar),
                    { dialog, which ->
                        onBackPressed()
                    }, false
                )
                return
            }
        }

        onResume()
    }

    override fun onResume() {
        super.onResume()
        setUpToolbar()
        setUpGalleryList()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onCheckedPermission()
    }

    private fun setUpToolbar() {
        binding.toolbarGallerySpot.toolbarNameGallerySpot.setTitle(R.string.gallery)
        setSupportActionBar( binding.toolbarGallerySpot.toolbarNameGallerySpot)
        binding.toolbarGallerySpot.toolbarNameGallerySpot.setBackgroundColor(Color.parseColor(SpotCameraConfig.buttonColor))
        binding.clGallery.setBackgroundColor(Color.parseColor(SpotCameraConfig.buttonColor))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.parseColor(SpotCameraConfig.buttonColor)
    }

    private fun setUpGalleryList() {
        val mLayoutManager = GridLayoutManager(
            this,
            2,
            GridLayoutManager.VERTICAL,
            false
        )
        allFolder = getListGallery("")
        mRecycle = RecyclerListGallery(this, allFolder!!)
        binding.recycleGallery.layoutManager = mLayoutManager
        binding.recycleGallery.addItemDecoration(RecycleItemDecoration(0))
        binding.recycleGallery.adapter = mRecycle
    }

    private fun getListGallery( folder: String) : ArrayList<FotoGaleria> {
        return when(nivel){
            0 -> {
                getImageFolderList()
            }
            else -> {
                getImages(folder)
            }
        }
    }

    private fun getImages(folder: String) : ArrayList<FotoGaleria> {
        allFolder!!.clear()

        val projection = arrayOf(
            MediaStore.Files.FileColumns._ID.replace("[ \u00A0]".toRegex(), ""),
            MediaStore.Files.FileColumns.DATA.trim().replace("[ \u00A0]".toRegex(), ""),
            MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME.trim().replace("[ \u00A0]".toRegex(), ""),
            MediaStore.Files.FileColumns.DATE_TAKEN.trim().replace("[ \u00A0]".toRegex(), ""),
            MediaStore.Files.FileColumns.MIME_TYPE.trim().replace("[ \u00A0]".toRegex(), "")
        )


        applicationContext.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
            MediaStore.Files.FileColumns.DATA + " like ? ",
            Array<String>(1) {"%${folder.trim().replace("[ \u00A0]".toRegex(), "")}%"}, MediaStore.Files.FileColumns.DATE_TAKEN.trim() + " DESC"
        )?.use { cursor ->
            while (cursor.moveToNext()) {
                if(cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE)).contains("jpeg") ||
                    cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE)).contains("jpg") ||
                    cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE)).contains("png") ||
                    cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE)).contains("pdf")) {
                    val fotoGaleria = FotoGaleria()
                    fotoGaleria.dataFile = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA))
                    fotoGaleria.isFolder = false
                    allFolder!!.add(fotoGaleria)
                }
            }
        }

        return allFolder!!
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val intent = Intent()
                setResult(Activity.RESULT_CANCELED, intent)
                onBackPressed()

            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        when(nivel){
            1 -> {
                binding.toolbarGallerySpot.toolbarNameGallerySpot.setTitle(R.string.gallery)
                nivel = 0
                getListGallery("")
                mRecycle!!.notifyDataSetChanged()
            }
            else -> finish()
        }
    }

    override fun onSelectImage(image: String) {
        val fotoAntiga = image

        val novaFoto =
                ImagemUtil.retornaCaminhoFotoURINew(Uri.fromFile(File(fotoAntiga)), 0, "TEMP_GALLERY", this)!!

        redimenAndRotateImage(File(novaFoto))


    }

    override fun onSelectFolder(folder: String) {
        nivel = 1
        getListGallery( folder)
        mRecycle!!.notifyDataSetChanged()
        binding.toolbarGallerySpot.toolbarNameGallerySpot.title = folder
    }

    private fun redimenAndRotateImage(arquivo: File) {
        GlobalScope.launch {
            try {
                val compressedImageFile =
                    Compressor.compress(this@SpotGallery, arquivo)

                val newImage = compressedImageFile.absolutePath
                callback(newImage)
                cancel()
            }catch (e: Exception) {
                cancel()
                onAlerta("Não foi possível selecionar o arquivo.")
                Log.e("ERRO_FOTO", e.message!!)
            }
        }
    }

    private fun callback(arquivonew: String?) {
        if(arquivonew.isNullOrEmpty()) {
            onAlerta("Não foi possível selecionar o arquivo.")
            return
        }

        val intent = Intent()
        intent.putExtra("arquivo", arquivonew)
        setResult(Activity.RESULT_OK, intent)
        nivel = 0
        onBackPressed()
    }

    private fun getImageFolderList() : java.util.ArrayList<FotoGaleria> {
        allFolder!!.clear()

        try {
            val projection = arrayOf(
                MediaStore.Images.Media.DATA.trim().replace("[ \u00A0]".toRegex(), ""),
                MediaStore.Images.Media._ID.trim().replace("[ \u00A0]".toRegex(), ""),
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME.trim().replace("[ \u00A0]".toRegex(), ""),
                MediaStore.Images.Media.DATE_TAKEN.trim().replace("[ \u00A0]".toRegex(), "")
            )
            val images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val orderBy = MediaStore.Images.Media.BUCKET_DISPLAY_NAME.trim()
            val cur: Cursor? = contentResolver.query(
                images, projection,  // Which
                // columns
                // to return
                null,  // Which rows to return (all rows)
                null,  // Selection arguments (none)
                "$orderBy" // Ordering
            )
            if (cur != null && cur.moveToFirst()) {
                val bucketColumn: Int = cur.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME.trim().replace("[ \u00A0]".toRegex(), ""))
                do {
                    cur.getString(bucketColumn)?.let {bucket ->
                        if (allFolder!!.none { it.dataFile == bucket } ) {
                            val fotoGaleria = FotoGaleria()
                            fotoGaleria.dataFile = bucket
                            fotoGaleria.isFolder = true
                            allFolder!!.add(fotoGaleria)
                        }
                    }

                } while (cur.moveToNext())
            }
        } catch (e: Exception) {
            val mensagem = String.format("%s\n\nMotivo: %s", "Não foi possível selecionar o arquivo", e.message.toString())
            onAlerta(mensagem)
        }

        return allFolder!!
    }

    private fun onAlerta(mensagem: String) {
        Alerta.show(this@SpotGallery, resources.getString(R.string.msg_atencao), mensagem, false)
    }
}