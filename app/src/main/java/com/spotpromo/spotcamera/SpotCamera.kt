package com.spotpromo.spotcamera

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.*
import android.view.animation.OvershootInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.ImageHeaderParser.UNKNOWN_ORIENTATION
import com.google.common.util.concurrent.ListenableFuture
import com.spotpromo.spotcamera.adapter.RecyclerGalleryCamera
import com.spotpromo.spotcamera.config.SpotCameraConfig
import com.spotpromo.spotcamera.databinding.LayoutSpotCameraBinding
import com.spotpromo.spotcamera.gallery.SpotGallery
import com.spotpromo.spotcamera.utils.Alerta
import com.spotpromo.spotcamera.utils.ImagemUtil.retornaCaminhoFotoURINew
import id.zelory.compressor.Compressor
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


open class SpotCamera : AppCompatActivity(), RecyclerGalleryCamera.ClickImageCamera {
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraExecutor: ExecutorService
    private var flashMode: Int = ImageCapture.FLASH_MODE_OFF
    private val CAMERA_REQUEST_CODE = 100
    private val GALLERY_REQUEST_CODE = 100
    private var ultimaRotacao = 0
    private var isAnimatingTakePicture = false
    private var isAnimatingFlash = false
    private var isAnimatingGallery = false
    private var isAnimatingSwitchCamera = false
    private var isFrontal = false
    private lateinit var binding: LayoutSpotCameraBinding
    private lateinit var preview: Preview
    private lateinit var camera: Camera


    private var imageCapture : ImageCapture? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutSpotCameraBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setFullScreen()
        hideSystemUI()
        setUpColors()
        onCheckedPermission()
        Log.e("........", "passou oncreate")
    }

    override fun onResume() {
        super.onResume()

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider, CameraSelector.LENS_FACING_BACK)
        }, ContextCompat.getMainExecutor(this))

        Log.e("........", "passou onresume")
    }

    private fun setUpColors() {
        binding.fabTakePicture.backgroundTintList =
            ColorStateList.valueOf(Color.parseColor(SpotCameraConfig.buttonColor))
        binding.fabTakePicture.imageTintList = ColorStateList.valueOf(Color.WHITE)
        binding.ivFlash.setColorFilter(Color.parseColor(SpotCameraConfig.buttonColor))
        binding.ivSwitchCamera.setColorFilter(Color.parseColor(SpotCameraConfig.buttonColor))
        binding.progressPhoto.progressTintList =
            ColorStateList.valueOf((Color.parseColor(SpotCameraConfig.buttonColor)))
        binding.progressPhoto.indeterminateTintList =
            ColorStateList.valueOf(Color.parseColor(SpotCameraConfig.buttonColor))
        binding.ivGallery.setColorFilter(Color.parseColor(SpotCameraConfig.buttonColor))
    }

    private fun onCheckedPermission() {
        if (ContextCompat.checkSelfPermission(
                this@SpotCamera,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(
                this@SpotCamera, getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_camera),
                resources.getString(R.string.btn_permitir),
                { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@SpotCamera,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                { dialog, which ->
                    onBackPressed()
                }, false
            )
            return
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this@SpotCamera,
                    Manifest.permission.READ_MEDIA_IMAGES
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Alerta.show(
                    this@SpotCamera, getString(R.string.msg_atencao),
                    resources.getString(R.string.msg_permissao_arquivos),
                    resources.getString(R.string.btn_permitir),
                    { dialog, which ->
                        ActivityCompat.requestPermissions(
                            this@SpotCamera,
                            arrayOf(Manifest.permission.READ_MEDIA_IMAGES),
                            GALLERY_REQUEST_CODE
                        )
                    },
                    resources.getString(R.string.btn_cancelar),
                    { dialog, which ->
                        onBackPressed()
                    }, false
                )
                return
            }

        } else {
            if (ContextCompat.checkSelfPermission(
                    this@SpotCamera,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Alerta.show(
                    this@SpotCamera, getString(R.string.msg_atencao),
                    resources.getString(R.string.msg_permissao_arquivos),
                    resources.getString(R.string.btn_permitir),
                    { dialog, which ->
                        ActivityCompat.requestPermissions(
                            this@SpotCamera,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            GALLERY_REQUEST_CODE
                        )
                    },
                    resources.getString(R.string.btn_cancelar),
                    { dialog, which ->
                        onBackPressed()
                    }, false
                )
                return
            }
        }

        onResume()
    }

    fun bindPreview(cameraProvider: ProcessCameraProvider, lensFacing: Int) {
        Log.e("........", "passou camera...")
        cameraProvider.unbindAll()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val screenWidth = 480
        val screenHeight = 720
        val previewSize: Size = Size(screenWidth, screenHeight)

        imageCapture = ImageCapture.Builder()
            .setFlashMode(flashMode)
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
            .setTargetResolution(previewSize!!)
            .build()


        preview = Preview.Builder()
            .build()

        val cameraSelector: CameraSelector = CameraSelector.Builder()
            .requireLensFacing(lensFacing)
            .build()

        preview.setSurfaceProvider( binding.texture.surfaceProvider)

//        val viewPort: ViewPort = ViewPort.Builder(
//            Rational(
//                binding.texture.width,
//                binding.texture.height
//            ),
//            binding.texture.display.rotation
//        ).build()

//        val useCaseGroup = UseCaseGroup.Builder()
//            .addUseCase(preview)
//            .addUseCase(imageAnalyzer)
//            .addUseCase(imageCapture!!)
//            .setViewPort(viewPort)
//            .build()

        //cameraProvider.unbindAll()

       // camera = cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, useCaseGroup)

        camera = cameraProvider.bindToLifecycle(
            this as LifecycleOwner,
            cameraSelector,
            preview,
            imageCapture
        )

        cameraExecutor = Executors.newSingleThreadExecutor()

        if (camera.cameraInfo.hasFlashUnit()) {
            setUpFlash()
            binding.ivFlash.visibility = View.VISIBLE
        } else
            binding.ivFlash.visibility = View.GONE

        setupZoom(camera)
        setUpTakePicture()
//        setUpGallery()
        setUpReverseCamera()
    }

    private fun setUpReverseCamera() {
        if (!checkCameraFront(this))
            binding.ivSwitchCamera.visibility = View.GONE
        else {
            binding.ivSwitchCamera.setOnClickListener {
                isFrontal = if (!isFrontal) {
                    bindPreview(cameraProviderFuture.get(), CameraSelector.LENS_FACING_FRONT)
                    true
                } else {
                    bindPreview(cameraProviderFuture.get(), CameraSelector.LENS_FACING_BACK)
                    false
                }
            }
        }
    }

    private fun setUpTakePicture() {
      // val file =  File(this.getExternalFilesDir(null).toString()  + "/temp_file.jpeg") //PARA TESTE
        val arquivo = File(SpotCameraConfig.tempFilePath)
       // val arquivo = file //PARA TESTE
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(arquivo)
            .build()

        binding.fabTakePicture.setOnClickListener {
            binding.progressPhoto.visibility = View.VISIBLE
            imageCapture!!.takePicture(outputFileOptions, cameraExecutor,
                object : ImageCapture.OnImageSavedCallback {
                    @SuppressLint("RestrictedApi")
                    override fun onImageSaved(img: ImageCapture.OutputFileResults) {
                        redimenAndRotateImage(arquivo)
                    }

                    override fun onError(error: ImageCaptureException) {
                        callbackerror(error.message ?: "Erro ao capturar a imagem", it)

                    }
                })
        }
    }

    private fun redimenAndRotateImage(arquivo: File) {

        GlobalScope.launch {
            try {
                val compressedImageFile =
                    Compressor.compress(this@SpotCamera, arquivo)

                val newImage = compressedImageFile.absolutePath
                callback(newImage)
                cancel()
            }catch (e: Exception) {
                cancel()
                onAlertaFoto()
                Log.e("ERRO_FOTO", e.message!!)
            }
        }
    }


    private fun onAlertaFoto() = Alerta.show(
        this@SpotCamera,
        resources.getString(R.string.msg_atencao),
        resources.getString(R.string.erro_foto),
        false
    )

    private fun callbackerror(message: String, view: View) {
        doAsync {
            activityUiThread {
                binding.progressPhoto.visibility = View.GONE
                onAlerta(message)
                //view.performClick()
            }
        }
    }

    private fun callback(arquivonew: String?) {
        finishGettingImage(arquivonew)
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setupZoom(camera: Camera) {

        val listener = object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val currentZoomRatio = camera.cameraInfo.zoomState.value?.zoomRatio ?: 0F

                val delta = detector.scaleFactor
                camera.cameraControl.setZoomRatio(currentZoomRatio * delta)
                return true
            }
        }

        val scaleGestureDetector = ScaleGestureDetector(this, listener)

        binding.texture.setOnTouchListener { _, event ->
            scaleGestureDetector.onTouchEvent(event)
            return@setOnTouchListener true
        }

    }

    private fun setUpFlash() {
        binding.ivFlash.setOnClickListener {
            if ( binding.progressPhoto.visibility == View.GONE) {
                when (flashMode) {
                    ImageCapture.FLASH_MODE_OFF -> {
                        flashMode = ImageCapture.FLASH_MODE_ON
                        binding.ivFlash.setImageDrawable(
                            ContextCompat.getDrawable(
                                this,
                                R.drawable.if_flash_on
                            )
                        )
                    }
                    ImageCapture.FLASH_MODE_ON -> {
                        flashMode = ImageCapture.FLASH_MODE_AUTO
                        binding.ivFlash.setImageDrawable(
                            ContextCompat.getDrawable(
                                this,
                                R.drawable.if_flash_auto
                            )
                        )
                    }
                    ImageCapture.FLASH_MODE_AUTO -> {
                        flashMode = ImageCapture.FLASH_MODE_OFF
                        binding.ivFlash.setImageDrawable(
                            ContextCompat.getDrawable(
                                this,
                                R.drawable.if_flash_off
                            )
                        )
                    }
                }
                bindPreview(
                    cameraProviderFuture.get(),
                    if (!isFrontal) CameraSelector.LENS_FACING_BACK else CameraSelector.LENS_FACING_FRONT
                )
            }
        }
    }

    override fun onBackPressed() {
        if ( binding.progressPhoto.visibility == View.GONE) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // Oculta a barra de navegação
                or View.SYSTEM_UI_FLAG_FULLSCREEN // Oculta a barra de status
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun setFullScreen() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onCheckedPermission()
    }

    private fun setUpGallery() {
        if (SpotCameraConfig.isWithGallery) {
            binding.ivGallery.setOnClickListener {
                val intent = Intent(this, SpotGallery::class.java)
                startActivityForResult(intent, 105)
            }

            setUpGalleryRecycler()
        } else {
//            ivGallery.visibility = View.GONE
//            rvGallery.visibility = View.GONE
        }
    }

    private fun setUpGalleryRecycler() {
        binding.rvGallery.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        binding.rvGallery.layoutManager = mLayoutManager
        binding.rvGallery.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.HORIZONTAL
            )
        )
        binding.rvGallery.adapter = RecyclerGalleryCamera(this, getListGallery())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == 105) {
            try {
                val foto = data!!.extras!!.getString("arquivo")!!

                val file = File(foto)
                if (file.isFile) {
                    redimenAndRotateImage(file)
                } else
                    onAlerta("Não foi possível criar o arquivo de foto.")
            } catch (e: Exception) {
                onAlerta("Não foi possível ler o arquivo, tente novamente!")
            }
        }
    }

    @SuppressLint("Range")
    private fun getListGallery(): ArrayList<String> {
        val itens = ArrayList<String>()

        val projection = arrayOf(
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA,
            MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Images.ImageColumns.DATE_TAKEN,
            MediaStore.Images.ImageColumns.MIME_TYPE
        )
        applicationContext.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
            null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC"
        )?.use { cursor ->
            while (cursor.moveToNext() && cursor.position < 20) {
                itens.add(cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)))
            }
        }

        return itens
    }

    private fun finishGettingImage(arquivo: String?) {

        if (arquivo.isNullOrEmpty()) {
            onAlerta("Não foi possível criar o arquivo.")
            return
        }

        val intent = Intent()
        intent.putExtra("arquivo", arquivo)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onSelectImage(image: String) {
        try {
            val fotoAntiga = image

            val novaFoto =
                retornaCaminhoFotoURINew(Uri.fromFile(File(fotoAntiga)), 0, "TEMP_GALLERY", this)!!
            val novoArquivo = File(novaFoto)

            if (novoArquivo.isFile) {
                redimenAndRotateImage(novoArquivo)
            } else onAlerta("Não foi possível criar o arquivo de foto.")
        } catch (e: Exception) {
            onAlerta("Não foi possível ler o arquivo, tente novamente!")
        }

    }

    private val orientationEventListener by lazy {
        object : OrientationEventListener(this) {
            override fun onOrientationChanged(orientation: Int) {
                if (orientation == UNKNOWN_ORIENTATION) {
                    return
                }

                val rotation = when (orientation) {
                    in 45 until 135 -> Surface.ROTATION_270
                    in 135 until 225 -> Surface.ROTATION_180
                    in 225 until 315 -> Surface.ROTATION_90
                    else -> Surface.ROTATION_0
                }

                val orientationRotarion = when (rotation) {
                    Surface.ROTATION_270 -> 270f
                    Surface.ROTATION_180 -> 180f
                    Surface.ROTATION_90 -> 90f
                    else -> 0f
                }

                if (binding.fabTakePicture != null && binding.ivFlash != null && binding.ivGallery != null)
                    rotateFab(orientationRotarion, rotation != ultimaRotacao)

                if (imageCapture != null)
                    imageCapture!!.targetRotation = rotation

                ultimaRotacao = rotation
            }
        }
    }

    override fun onStart() {
        super.onStart()
        setListener(false)
    }

    override fun onStop() {
        super.onStop()
        setListener(true)
    }

    private fun setListener(isDisabled: Boolean) {
        if (isDisabled)
            orientationEventListener.disable()
        else
            orientationEventListener.enable()
    }

    open fun rotateFab(rotacao: Float, rotate: Boolean) {
        if (rotate && !isAnimatingTakePicture && !isAnimatingFlash && !isAnimatingGallery && !isAnimatingSwitchCamera) {
            isAnimatingTakePicture = true
            isAnimatingFlash = true
            isAnimatingGallery = true
            isAnimatingSwitchCamera = true

            val interpolator = OvershootInterpolator()
            ViewCompat.animate( binding.fabTakePicture).rotation(rotacao).withLayer().setDuration(500)
                .setInterpolator(interpolator).withEndAction {
                    isAnimatingTakePicture = false
                }.start()

            ViewCompat.animate( binding.ivFlash).rotation(rotacao).withLayer().setDuration(500)
                .setInterpolator(interpolator).withEndAction {
                    isAnimatingFlash = false
                }.start()

            ViewCompat.animate( binding.ivGallery).rotation(rotacao).withLayer().setDuration(500)
                .setInterpolator(interpolator).withEndAction {
                    isAnimatingGallery = false
                }.start()

            ViewCompat.animate( binding.ivSwitchCamera).rotation(rotacao).withLayer().setDuration(500)
                .setInterpolator(interpolator).withEndAction {
                    isAnimatingSwitchCamera = false
                }.start()
        }
    }

    fun checkCameraFront(context: Context): Boolean {
        return context.getPackageManager()
            .hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)
    }

    private fun onAlerta(mensagem: String) {
        Alerta.show(this@SpotCamera, resources.getString(R.string.msg_atencao), mensagem, false)
    }


}